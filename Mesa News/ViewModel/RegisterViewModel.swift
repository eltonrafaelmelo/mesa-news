//
//  RegisterViewModel.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import Foundation
import UIKit

protocol RegisterViewModelProtocol {
    var RegisterFinish: ((Bool, Bool, String) -> Void)? { get set }
}

class RegisterViewModel: RegisterViewModelProtocol {
    
    var RegisterFinish: ((Bool, Bool, String) -> Void)?
    
    var registerResponse: GenericResponse? {
        didSet {
            self.RegisterFinish?(true, false, "Cadastro realizado com sucesso.")
        }
    }
    
    func postRegister(name: String, email: String, password: String){
        RestClient.postRegister(name: name, email: email, senha: password, completionHandle: { loginResponse, erro in
            if erro == nil {
                self.registerResponse = loginResponse
            } else {
                self.RegisterFinish?(false, true, erro!.localizedDescription)
            }
        })
    }
    
    
}
