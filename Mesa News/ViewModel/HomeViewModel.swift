//
//  HomeViewModel.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import Foundation
import UIKit

protocol HomeViewModelProtocol {
    var getNewsFinish: ((Bool, Bool, String) -> Void)? { get set }
}

class HomeViewModel: HomeViewModelProtocol {
    
    var getNewsFinish: ((Bool, Bool, String) -> Void)?
    
    var listaFiltrada:[Noticia] = []
    
    var newsResponse: NoticiasResponse? {
        didSet {
            self.listaFiltrada = newsResponse!.data
            self.getNewsFinish?(true, false, "")
        }
    }
    
    func getNoticias() {
        RestClient.getNews(data: "", completionHandle: {response, erro in
            if erro == nil {
                self.newsResponse = response
            } else {
                self.getNewsFinish?(true, false, erro!.localizedDescription)
            }
        })
    }
}
