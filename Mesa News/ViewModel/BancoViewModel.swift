//
//  BancoViewModel.swift
//  Mesa News
//
//  Created by Elton Melo on 29/01/21.
//

import Foundation
import UIKit
import RealmSwift


protocol BancoViewModelProtocol {
    var noticiaFavoritada: ((Bool, Bool) -> Void)? { get set }
    var noticiaFavoritadaComSucesso: ((Bool, Bool) -> Void)? { get set }
    var noticiaDesfavoritadaComSucesso: ((Bool, Bool) -> Void)? { get set }
    var listouNoticiasFavoritadas: ((Bool, Bool) -> Void)? { get set }
    var getNewsFinish: ((Bool, Bool, String) -> Void)? { get set }
}

class BancoViewModel: BancoViewModelProtocol {
    var getNewsFinish: ((Bool, Bool, String) -> Void)?
    
    var listouNoticiasFavoritadas: ((Bool, Bool) -> Void)?
    
    var noticiaFavoritadaComSucesso: ((Bool, Bool) -> Void)?
    
    var noticiaDesfavoritadaComSucesso: ((Bool, Bool) -> Void)?
    
    var noticiaFavoritada: ((Bool, Bool) -> Void)?
    
    var listaFavoritos: Results<Noticia>!
    
    var listaFinal:[Noticia] = []
    
    var listaTemp:[Noticia] = []
    
    var tableView: UITableViewController!
    
    var view : UIViewController!
    
    var newsResponse: NoticiasResponse? {
        didSet {
            self.listaFinal = newsResponse!.data
            if self.listaFinal.count == 0 {
                self.getNewsFinish?(false, true, "Sem noticias para essa data!")
            } else {
                self.getNewsFinish?(true, false, "")
            }
            
        }
    }
    
    func getNoticiaFavoritada(titulo: String) {
        if tableView == nil && view != nil {
            if view.achouNoticia(titulo: titulo) {
                self.noticiaFavoritada?(true, false)
            } else {
                self.noticiaFavoritada?(false, false)
            }
        } else {
            if tableView.achouNoticia(titulo: titulo) {
                self.noticiaFavoritada?(true, false)
            } else {
                self.noticiaFavoritada?(false, false)
            }
        }
        
    }
    
    func favoritarNoticia(noticia: Noticia) {
        if tableView == nil && view != nil {
            view.salvarNoticia(noticia: noticia)
        } else {
            tableView.salvarNoticia(noticia: noticia)
        }
        self.noticiaFavoritadaComSucesso?(true,false)
    }
    
    func desfavoritarNoticia(noticia: Noticia) {
        if tableView == nil && view != nil {
            view.deleteNoticia(noticia: noticia)
        } else {
            tableView.deleteNoticia(noticia: noticia)
        }
        self.noticiaDesfavoritadaComSucesso?(false,false)
    }
    
    func getListaDeFavoritos() {
        listaFinal = [Noticia]()
        if tableView == nil && view != nil {
            self.listaFavoritos = view.getListDeNoticias()
        } else {
            self.listaFavoritos = tableView.getListDeNoticias()
        }
        
        if self.listaFavoritos != nil && self.listaFavoritos.count > 0 {
            for item in self.listaFavoritos {
                self.listaFinal.append(item)
                self.listaTemp.append(item)
            }
            self.listouNoticiasFavoritadas?(true, false)
        } else {
            self.listouNoticiasFavoritadas?(false, true)
        }
    }
    
    func getNoticiasPorData(data: String) {
        listaFinal = [Noticia]()
        self.getNewsFinish?(true, false, "")
        RestClient.getNews(data: data, completionHandle: {response, erro in
            if erro == nil {
                self.newsResponse = response
                self.listaTemp = response!.data
            } else {
                self.getNewsFinish?(true, false, erro!.localizedDescription)
            }
        })
    }
    
    func getListVazia() {
        listaFinal = [Noticia]()
        self.getNewsFinish?(true, false, "")
    }
    
}

