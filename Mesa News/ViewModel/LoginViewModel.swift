//
//  LoginViewModel.swift
//  Mesa News
//
//  Created by Elton Melo on 25/01/21.
//

import Foundation
import UIKit

protocol LoginViewModelProtocol {
    var loginFinish: ((Bool, Bool, String) -> Void)? { get set }
}

class LoginViewModel: LoginViewModelProtocol {
    
    var loginFinish: ((Bool, Bool, String) -> Void)?

    var loginResponse: GenericResponse? {
        didSet {
            self.loginFinish?(true, false, "")
        }
    }
    
    func postLogin(email: String, password: String){
        RestClient.postLogin(email: email, senha: password, completionHandle: { loginResponse, erro in
            if erro == nil {
                self.loginResponse = loginResponse
                UserDefaults.standard.setToken(token: (self.loginResponse?.token)!)
            } else {
                self.loginFinish?(false, true, erro!.localizedDescription)
            }
        })
    }
    
}
