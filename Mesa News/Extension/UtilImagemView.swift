//
//  UtilImagemView.swift
//  Mesa News
//
//  Created by Elton Melo on 27/01/21.
//

import Foundation
import UIKit

extension UIImageView {

    private static var taskKey = 0
    private static var urlKey = 0

    private var activityIndicator: UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.blue
        self.addSubview(activityIndicator)

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false

        let centerX = NSLayoutConstraint(item: self,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: activityIndicator,
                                         attribute: .centerX,
                                         multiplier: 1,
                                         constant: 0)
        let centerY = NSLayoutConstraint(item: self,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: activityIndicator,
                                         attribute: .centerY,
                                         multiplier: 1,
                                         constant: 0)
        self.addConstraints([centerX, centerY])
        return activityIndicator
    }

    private var currentTask: URLSessionTask? {
        get { return objc_getAssociatedObject(self, &UIImageView.taskKey) as? URLSessionTask }
        set { objc_setAssociatedObject(self, &UIImageView.taskKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    private var currentURL: URL? {
        get { return objc_getAssociatedObject(self, &UIImageView.urlKey) as? URL }
        set { objc_setAssociatedObject(self, &UIImageView.urlKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    func loadImageAsync(with urlString: String?) {

        weak var oldTask = currentTask
        currentTask = nil
        oldTask?.cancel()

        self.image = nil

        guard let urlString = urlString else { return }

        // check cache
        if let cachedImage = ImageCache.shared.image(forKey: urlString) {
            self.image = cachedImage
            return
        }

        // download
        let url = URL(string: urlString)!
        currentURL = url

        let activityIndicator = self.activityIndicator

        DispatchQueue.main.async {
            activityIndicator.startAnimating()
        }

        DispatchQueue.main.async {
            self.image = UIImage(named: "logoNews")
        }

        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            
            if self != nil {
                DispatchQueue.main.async {
                   self!.contentMode = .scaleAspectFill
                }
            }
            
            self?.currentTask = nil

            //error handling
            if let error = error {
                // don't bother reporting cancelation errors
                if (error as NSError).domain == NSURLErrorDomain && (error as NSError).code == NSURLErrorCancelled {
                    return
                }

                print(error)
                return
            }

            guard let data = data, let downloadedImage = UIImage(data: data) else {
                print("unable to extract image")
                return
            }

            ImageCache.shared.save(image: downloadedImage, forKey: urlString)
            
            if url == self?.currentURL {
                DispatchQueue.main.async {
                    
                    self?.image = downloadedImage
                    activityIndicator.stopAnimating()
                    activityIndicator.removeFromSuperview()
                }
            }
        }

        // save and start new task
        currentTask = task
        task.resume()
    }

    class ImageCache {
        private let cache = NSCache<NSString, UIImage>()
        private var observer: NSObjectProtocol!

        static let shared = ImageCache()

        private init() {
            // make sure to purge cache on memory pressure didReceiveMemoryWarningNotification

            observer = NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil) { [weak self] notification in
                self?.cache.removeAllObjects()
            }
        }

        deinit {
            NotificationCenter.default.removeObserver(observer!)
        }

        func image(forKey key: String) -> UIImage? {
            return cache.object(forKey: key as NSString)
        }

        func save(image: UIImage, forKey key: String) {
            cache.setObject(image, forKey: key as NSString)
        }
    }

}
