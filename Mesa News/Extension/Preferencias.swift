//
//  Preferencias.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import Foundation

extension UserDefaults {
    
    func getToken()->String{
        var result = ""
        let valor = UserDefaults.standard.string(forKey: "TOKEN")
        result = valor != nil && !valor!.isEmpty ? valor ?? "" : ""
        return result
    }
    
    func setToken(token: String){
        UserDefaults.standard.set(token, forKey: "TOKEN")
    }
    
    func isUserLogado() -> Bool {
        return getToken() == "" ? false : true
    }
}
