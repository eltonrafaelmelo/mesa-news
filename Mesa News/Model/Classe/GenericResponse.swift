//
//  LoginResponse.swift
//  Mesa News
//
//  Created by Elton Melo on 25/01/21.
//

import UIKit
import Foundation
import ObjectMapper

class GenericResponse: NSObject, Mappable {
    
    var token: String!
    
    override init() {
        super.init()
    }
    
    static func newInstance(map: Map) -> Mappable? {
        return GenericResponse()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        token   <- map["token"]
    }

}
