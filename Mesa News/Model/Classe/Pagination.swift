//
//  Pagination.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import UIKit
import Foundation
import ObjectMapper

class Pagination: NSObject, Mappable {
    
    var current_page: Int!
    var per_page: Int!
    var total_pages: Int!
    var total_items: Int!
    
    override init() {
        super.init()
    }
    
    static func newInstance(map: Map) -> Mappable? {
        return Pagination()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        current_page  <- map["current_page"]
        per_page      <- map["per_page"]
        total_pages   <- map["total_pages"]
        total_items   <- map["total_items"]
    }

}
