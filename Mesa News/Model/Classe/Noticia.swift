//
//  Noticia.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import UIKit
import Foundation
import ObjectMapper
import RealmSwift

class Noticia:  Object, Mappable {
    
    @objc dynamic var id = Int.random(in: 0 ... 1000000)
    @objc dynamic var title: String!
    @objc dynamic var descriptionNew: String!
    @objc dynamic var content: String!
    @objc dynamic var author: String!
    @objc dynamic var published_at: String!
    @objc dynamic var highlight = false
    @objc dynamic var url: String!
    @objc dynamic var image_url: String!
    
    override static func primaryKey() -> String? {
           return "id"
       }
    
    required init() {
        super.init()
    }
    
    static func newInstance(map: Map) -> Mappable? {
        return Noticia()
    }
    
    required init?(map: Map) {
        
    }
    
    init(noticia: Noticia) {
        title = noticia.title
        descriptionNew = noticia.descriptionNew
        content = noticia.content
        author = noticia.author
        published_at = noticia.published_at
        highlight = noticia.highlight
        url = noticia.url
        image_url = noticia.image_url
    }
    
    func mapping(map: Map) {
        title           <- map["title"]
        descriptionNew  <- map["description"]
        content         <- map["content"]
        author          <- map["author"]
        published_at    <- map["published_at"]
        highlight       <- map["highlight"]
        url             <- map["url"]
        image_url       <- map["image_url"]
    }

}
