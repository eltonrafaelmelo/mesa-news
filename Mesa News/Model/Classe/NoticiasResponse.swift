//
//  NoticiasResponse.swift
//  Mesa News
//
//  Created by Elton Melo on 27/01/21.
//

import UIKit
import Foundation
import ObjectMapper

class NoticiasResponse: NSObject, Mappable {

    var pagination: Pagination!
    var data: [Noticia] = []
    
    override init() {
        super.init()
    }
    
    static func newInstance(map: Map) -> Mappable? {
        return NoticiasResponse()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        pagination   <- map["pagination"]
        data         <- map["data"]
    }
    
}
