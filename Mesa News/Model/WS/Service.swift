//
//  Service.swift
//  Mesa News
//
//  Created by Elton Melo on 25/01/21.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

class Service: NSObject {
    
    func request(_ method: HTTPMethod, URLString: String, parameters: [String: Any]?, encoding: JSONEncoding, headers: [String: String]? = nil) -> DataRequest {
        
        print("URL: \(method.rawValue), \(URLString)")
        
        return AF.request(URLString, method: method, parameters: parameters, encoding:encoding , headers:[
            "Authorization": "Bearer \(UserDefaults.standard.getToken())",
        ]).response { (response) in
            print("RETORNO STATUS: \(String(describing: response.response?.statusCode))")
            print("RETORNO HEADERS: \(String(describing: response.response?.headers))")
        }
    }
}
