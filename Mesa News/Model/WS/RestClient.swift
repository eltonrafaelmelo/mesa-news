//
//  RestClient.swift
//  Mesa News
//
//  Created by Elton Melo on 25/01/21.
//

import Foundation
import ObjectMapper
import Alamofire
import SwiftyJSON

class RestClient {
    
    static let WS_HOST = "https://mesa-news-api.herokuapp.com" // podução
    static let urlLogin = "\(WS_HOST)/v1/client/auth/signin"
    static let urlCadastro = "\(WS_HOST)/v1/client/auth/signup"
    static let urlNoticias = "\(WS_HOST)/v1/client/news?"
    
    class func postLogin(email: String, senha: String, completionHandle:@escaping (_ response: GenericResponse?, _ error: Error?) -> ()){
        
        let serv = Service()
        
        let parametros = [
            "email": email as Any,
            "password": senha as Any
        ] as [String : Any]
        
        serv.request(.post, URLString: urlLogin, parameters: parametros, encoding: .default, headers: nil).responseData(completionHandler: { response in
            
            switch response.result {
            case .success(let json):
                if response.response?.statusCode == 200  || response.response?.statusCode == 201 || response.response?.statusCode == 204{
                    print("Login efetuado com Sucesso")
                    let jsonString = dataToJSON(data: json)
                    let loginResponse = Mapper<GenericResponse>().map(JSONObject: jsonString)
                    print(loginResponse as Any)
                    completionHandle(loginResponse,nil)
                } else if response.response?.statusCode == 401{
                    print("ERRO  Login: \(String(describing: response.response?.statusCode))")
                    let erro: Error = AppError.network(type: .custom(errorCode: response.response?.statusCode, errorDescription: "E-mail ou Senha incorreto!"))
                    completionHandle(nil,erro)
                } else {
                    print("ERRO  Salvar foto: \(String(describing: response.response?.statusCode))")
                    let erro: Error = AppError.network(type: .custom(errorCode: response.response?.statusCode, errorDescription: "Desculpe o transtorne, tente novamente!"))
                    completionHandle(nil,erro)
                }
                
            case .failure(let erro):
                print("ERROR: \(erro)")
                completionHandle(nil,response.error as NSError?)
            }
            
        })
    }
    
    class func postRegister(name: String, email: String, senha: String, completionHandle:@escaping (_ response: GenericResponse?, _ error: Error?) -> ()){
        
        let serv = Service()
        
        let parametros = [
            "name": email as Any,
            "email": email as Any,
            "password": senha as Any
        ] as [String : Any]
        
        serv.request(.post, URLString: urlCadastro, parameters: parametros, encoding: .default, headers: nil).responseData(completionHandler: { response in
            
            switch response.result {
            case .success(let json):
                if response.response?.statusCode == 200  || response.response?.statusCode == 201 || response.response?.statusCode == 204{
                    print("Cadastro efetuado com Sucesso")
                    let jsonString = dataToJSON(data: json)
                    let loginResponse = Mapper<GenericResponse>().map(JSONObject: jsonString)
                    print(loginResponse as Any)
                    completionHandle(loginResponse,nil)
                } else if response.response?.statusCode == 422{
                    print("ERRO  Cadastro: \(String(describing: response.response?.statusCode))")
                    let erro: Error = AppError.network(type: .custom(errorCode: response.response?.statusCode, errorDescription: "E-mail já cadastrado!"))
                    completionHandle(nil,erro)
                } else {
                    print("ERRO: \(String(describing: response.response?.statusCode))")
                    let erro: Error = AppError.network(type: .custom(errorCode: response.response?.statusCode, errorDescription: "Desculpe o transtorne, tente novamente!"))
                    completionHandle(nil,erro)
                }
                
            case .failure(let erro):
                print("ERROR: \(erro)")
                completionHandle(nil,response.error as NSError?)
            }
            
        })
    }
    
    class func getNews(data: String, completionHandle:@escaping (_ response: NoticiasResponse?, _ error: Error?) -> ()){
        
        let urlFinal = "\(urlNoticias)current_page1=&per_page=20&published_at=\(data)"

        let serv = Service()
        
        serv.request(.get, URLString: urlFinal, parameters: nil, encoding: .default, headers: nil).responseData(completionHandler: { response in
            
            switch response.result {
            case .success(let json):
                if response.response?.statusCode == 200  || response.response?.statusCode == 201 || response.response?.statusCode == 204{
                    print("Noticias listada com Sucesso")
                    let jsonString = dataToJSON(data: json)
                    let loginResponse = Mapper<NoticiasResponse>().map(JSONObject: jsonString)
                    print(loginResponse as Any)
                    completionHandle(loginResponse,nil)
                }else if response.response?.statusCode == 401{
                    print("ERRO: \(String(describing: response.response?.statusCode))")
                    let erro: Error = AppError.network(type: .custom(errorCode: response.response?.statusCode, errorDescription: "Usuário não logado!"))
                    completionHandle(nil,erro)
                } else {
                    print("ERRO: \(String(describing: response.response?.statusCode))")
                    let erro: Error = AppError.network(type: .custom(errorCode: response.response?.statusCode, errorDescription: "Desculpe o transtorne, tente novamente!"))
                    completionHandle(nil,erro)
                }
                
            case .failure(let erro):
                print("ERROR: \(erro)")
                completionHandle(nil,response.error as NSError?)
            }
            
        })
    }
    
    
    
    class func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
}
