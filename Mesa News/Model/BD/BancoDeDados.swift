//
//  BancoDeDados.swift
//  Mesa News
//
//  Created by Elton Melo on 28/01/21.
//

import Foundation
import UIKit
import RealmSwift


let realm = try! Realm()

extension UIViewController {
    
    func salvarNoticia(noticia: Noticia) {
        
        let noticiaAdd = Noticia(noticia: noticia)
        
        print("ID da noticia: \(noticiaAdd.id)")
                
        try! realm.write {
            
            let listNoticiaTemp = realm.objects(Noticia.self)
            
            if (listNoticiaTemp.count > 0) {
                
                if achouNoticia(titulo: noticiaAdd.title) {
                    
                    print("não salva no banco")
                    
                } else {
                    
                    realm.add(noticiaAdd)
                    
                }
                
            } else {
                
                realm.add(noticiaAdd)
                
            }
            
        }
    }
    
    func achouNoticia(titulo: String)->Bool{
        
        var result = false
        
        let listNoticiaTemp = realm.objects(Noticia.self)
        
        for noticia in listNoticiaTemp {
            
            if noticia.title == titulo {
                
                result = true
            }
            
        }
        
        return result
    }
    
    func getListDeNoticia2(titulo: String)->Noticia{
        
        var result: Noticia!
        
        let listNoticiaTemp = realm.objects(Noticia.self)
        
        for noticia in listNoticiaTemp {
            
            if noticia.title == titulo {
                
                result = noticia
            }
            
        }
        
        return result
    }
    
    func deleteDB(){
        try! realm.write {
            realm.deleteAll()
        }
        
    }
    
    func getListDeNoticias()->Results<Noticia>{
        let listLicencasTemp = realm.objects(Noticia.self)
        return listLicencasTemp
    }
    
    func getNoticia(titulo: String)->Noticia{
        var result: Noticia = Noticia()
        result = realm.objects(Noticia.self).filter("title == \(titulo)").first!
        return result
    }
    
    func deleteNoticia(noticia: Noticia){
        let titulo = noticia.title
        let noticiaDelete = getListDeNoticia2(titulo: titulo!)
        try! realm.write {
            print("Deletou noticia: \(noticia.id)")
            realm.delete(noticiaDelete)
            
        }
    }
}
