//
//  NoticiaTableViewCell.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import UIKit

protocol NoticiaTableViewCellProtocol:class {
    func atualizaViewCell()
}

class NoticiaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelDescricao: UILabel!
    @IBOutlet weak var imageNoticia: UIImageView!
    @IBOutlet weak var buttonFavorito: UIButton!
    var favorito:Bool = false
    let viewModel = BancoViewModel()
    weak var delegate: NoticiaTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareViewModelObserver()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func touchButtonFavorito(_ sender: Any) {
        favorito = favorito ? false : true
        if favorito {
            self.viewModel.favoritarNoticia(noticia: noticiaItem!)
        } else {
            self.viewModel.desfavoritarNoticia(noticia: noticiaItem!)
        }
    }
    
    func reloadFavorito(){
        buttonFavorito.setBackgroundImage(!favorito ? UIImage(named: "favorito.png") :UIImage(named: "favoritoSelecionado.png"), for: .normal)
        
    }
    
    var noticiaItem: Noticia? {
        didSet {
            if let noticia = noticiaItem {
                labelTitulo.text = noticia.title ?? ""
                labelDescricao.text = noticia.descriptionNew ?? ""
                imageNoticia.loadImageAsync(with: noticia.image_url)
                viewModel.getNoticiaFavoritada(titulo: noticia.title)                
            }
        }
    }
    
    func prepareViewModelObserver() {
        
        self.viewModel.noticiaFavoritada = { (finished, error) in
            if !error {
                self.favorito = finished
                self.reloadFavorito()
            } else {
                self.favorito = finished
                self.reloadFavorito()
            }
        }
        
        self.viewModel.noticiaFavoritadaComSucesso = { (finished, error) in
            if !error {
                self.favorito = finished
                self.reloadFavorito()
            } else {
                self.favorito = finished
                self.reloadFavorito()
            }
        }
        
        self.viewModel.noticiaDesfavoritadaComSucesso = { (finished, error) in
            if !error {
                self.favorito = finished
                self.reloadFavorito()
            } else {
                self.favorito = finished
                self.reloadFavorito()
            }
            self.delegate?.atualizaViewCell()
        }
    }
    
    var tableView: UITableViewController? {
        didSet {
            if let table = tableView {
                self.viewModel.tableView = table
            }
        }
    }
    
    var viewController: UIViewController? {
        didSet {
            if let view = viewController {
                self.viewModel.view = view
            }
        }
    }
    
}
