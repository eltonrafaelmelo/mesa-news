//
//  SplashViewController.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.isUserLogado() {
            self.performSegue(withIdentifier: "home", sender: nil)
        } else {
            self.performSegue(withIdentifier: "login", sender: nil)
        }
    }

}
