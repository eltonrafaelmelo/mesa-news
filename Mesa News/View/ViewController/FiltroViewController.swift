//
//  FiltroViewController.swift
//  Mesa News
//
//  Created by Elton Melo on 29/01/21.
//

import UIKit

class FiltroViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource,  UITextFieldDelegate, UITableViewDelegate, NoticiaTableViewCellProtocol {
    
    @IBOutlet weak var textfielData: UITextField!
    let DBViewModel = BancoViewModel()
    let searchBar = UISearchBar(frame: CGRect.zero)
    @IBOutlet weak var tableView: UITableView!
    var noticiaSelecionada: Noticia!
    var opcaoSelecionada = 0
    var openPickerData = false
    let datePicker = UIDatePicker()
    var dataSelecionada = ""
    var msgListaVazia = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButton()
        loadInfos()
        prepareViewModelObserver()
        DBViewModel.view = self
        showDatePicker()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if opcaoSelecionada == 1 {
            DBViewModel.getListaDeFavoritos()
        } else {
            tableView.reloadData()
        }
    }
    
    func loadInfos() {
        searchBar.delegate = self
        searchBar.placeholder = "Pesquisar Notícias"
        searchBar.showsCancelButton = true
        tableView.delegate = self
        tableView.dataSource = self
        
        self.textfielData.delegate = self
        self.textfielData.setRightViewIcon2(icon: UIImage(named:"chevronDown")!)

    }
    
    func showDatePicker(){
        tableView.isHidden = true
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Ok", style: UIBarButtonItem.Style.done, target: self, action: #selector(donedatePicker))
        
        doneButton.tintColor = .orange
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItem.Style.done, target: self, action:  #selector(cancelDatePicker))
        
        cancelButton.tintColor = .red

        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        textfielData.inputAccessoryView = toolbar
        textfielData.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        tableView.isHidden = false
        openPickerData = false
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let formatterView = DateFormatter()
        formatterView.dateFormat = "dd/MM/yyyy"
        textfielData.text = formatterView.string(from: datePicker.date)
        let dataRest = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        showLoading(msg: "")
        DBViewModel.getNoticiasPorData(data: dataRest)
    }
    
    @objc func cancelDatePicker(){
        tableView.isHidden = false
        openPickerData = false
        self.view.endEditing(true)
    }
    
    func prepareViewModelObserver() {
        self.DBViewModel.listouNoticiasFavoritadas = {(finished, error) in
            if !error {
                print("Lista do banco local: \(self.DBViewModel.listaFinal.count)")
            } else {
                print("Lista de favoritos vazia")
                self.msgListaVazia = "Lista de favoritos vazia"
            }
            self.tableView.reloadData()
        }
        
        self.DBViewModel.getNewsFinish = { (finished, error, msg) in
            self.hideLoading()
            if !error {
                self.tableView.reloadData()
            } else {
                self.msgListaVazia = msg
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func touchSegmente(_ sender: UISegmentedControl) {
        
        print("Opção selecionada: \(sender.selectedSegmentIndex)")
        
        opcaoSelecionada = sender.selectedSegmentIndex
        
        if sender.selectedSegmentIndex == 0 {
            msgListaVazia = ""
            textfielData.text = ""
            showTextData()
            DBViewModel.getListVazia()
        } else {
            tableView.isHidden = false
            DBViewModel.getListaDeFavoritos()
            hideTextData()
        }
    }
    
    func showSearchBar() {
        
        UIView.animate(withDuration: 1.0, animations: {
            self.searchBar.alpha = 0
            self.navigationItem.titleView = self.searchBar
            self.removerButtonNavigationBarRight()
            self.searchBar.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        UIView.animate(withDuration: 1.0, animations: {
            self.navigationItem.titleView = nil
            self.title = "Filtro"
            self.addButton()
            self.searchBar.endEditing(true)
        }, completion: { finished in
            if self.opcaoSelecionada == 0 {
                self.DBViewModel.getNoticiasPorData(data: "")
            } else {
                self.DBViewModel.getListaDeFavoritos()
            }
        })
    }
    
    @IBAction func touchOpenData(_ sender: Any) {
        view.endEditing(true)
        if openPickerData == false{
            openPickerData = true
            showDatePicker()
        }
    }
    
    func showTextData() {
        UIView.animate(withDuration: 1.0, animations: {
            self.textfielData.alpha = 0
            self.textfielData.alpha = 1
            self.textfielData.isHidden = false
        }, completion: { finished in

        })
    }
    
    func hideTextData() {
        UIView.animate(withDuration: 1.0, animations: {
            self.textfielData.isHidden = true
            self.textfielData.endEditing(true)
        }, completion: { finished in
            
        })
    }
    
    func addButton(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(abrirPesquisa(sender:)))
    }
    
    @objc func abrirPesquisa(sender: UIBarButtonItem) {
        showSearchBar()
    }
    
    
    // MARK: - SeacherBar Delegate
  
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (self.searchBar.text!.isEmpty) {
            DBViewModel.listaFinal = DBViewModel.listaTemp
        } else {
            DBViewModel.listaFinal = (DBViewModel.listaTemp.filter({(midia: Noticia) -> Bool in
                let stringMatch = midia.title.lowercased().range(of:searchText.lowercased())
                let result = stringMatch != nil ? true : false
                return result
            }))
        }
        self.tableView.reloadData()
    }
    
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DBViewModel.listaFinal.count == 0 {
            self.tableView.setEmptyMessage(msgListaVazia)
        } else {
            self.tableView.restore()
        }
        return DBViewModel.listaFinal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticiaFiltroTableViewCell", for: indexPath) as! NoticiaTableViewCell
        let noticia = DBViewModel.listaFinal[indexPath.row]
        cell.viewController = self
        cell.noticiaItem = noticia
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        noticiaSelecionada = DBViewModel.listaFinal[indexPath.row]
        performSegue(withIdentifier: "filtroDetalhe", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filtroDetalhe" {
            if let destinationVC = segue.destination as? DetalheNoticiaViewController {
                destinationVC.noticia = noticiaSelecionada
            }
        }
    }
    
    // MARK: - Delegate Cell
    func atualizaViewCell() {
        if opcaoSelecionada == 1 {
            DBViewModel.getListaDeFavoritos()
        }
    }

}
