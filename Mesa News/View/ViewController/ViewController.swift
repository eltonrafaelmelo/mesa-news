//
//  ViewController.swift
//  Mesa News
//
//  Created by Elton Melo on 24/01/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textFieldLogin: UITextField!
    @IBOutlet weak var textFieldSenha: UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonCadastrar: UIButton!
    let viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        configTextField()
        prepareViewModelObserver()
    }
    
    func configTextField(){
        self.buttonLogin.layer.cornerRadius = 0.5 * self.buttonLogin.frame.size.height;
        self.buttonLogin.clipsToBounds = true;
        self.buttonCadastrar.layer.cornerRadius = 0.5 * self.buttonCadastrar.frame.size.height;
        self.buttonCadastrar.clipsToBounds = true;
        textFieldLogin.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        textFieldSenha.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
    }
    
    @IBAction func touchButtonLogin(_ sender: Any) {
        if (textFieldLogin.text == "") {
            showAlert(title: "Atenção", message: "Campo e-mail está vázio!", namebutton: "ok", completionHandle: { button in
                
            })
        } else if (isValidEmail(textFieldLogin.text!) == false) {
            showAlert(title:"Atenção", message: "E-mail inválido!", namebutton: "ok", completionHandle: { button in
                
            })
        } else if (textFieldSenha.text == "") {
            showAlert(title: "Atenção", message: "Campo senha está vázio!" , namebutton: "ok", completionHandle: { button in
                
            })
        } else {
            postLogin()
        }
    }
    
    func postLogin() {
        showLoading(msg: "")
        viewModel.postLogin(email: textFieldLogin.text!, password: textFieldSenha.text!)
    }
    
    func prepareViewModelObserver() {
        self.viewModel.loginFinish = { (finished, error, msg) in
            self.hideLoading()
            if !error {
                self.performSegue(withIdentifier: "screenHome", sender: nil)
            } else {
                self.showAlert(title: "Atenção", message: msg , namebutton: "ok", completionHandle: { button in
                    
                })
            }
        }
    }
    
    
}

