//
//  DetalheNoticiaViewController.swift
//  Mesa News
//
//  Created by Elton Melo on 27/01/21.
//

import UIKit
import WebKit

class DetalheNoticiaViewController: UIViewController {

    @IBOutlet weak var webViewNoticia: WKWebView!
    @IBOutlet weak var buttonFavorito: UIBarButtonItem!
    let viewModel = BancoViewModel()
    var favorito:Bool = false
    
    var noticia: Noticia!
    override func viewDidLoad() {
        super.viewDidLoad()

        if noticia != nil {
            loadWebView()
            prepareViewModelObserver()
            viewModel.view = self
            viewModel.getNoticiaFavoritada(titulo: noticia.title)
        }
    }
    
    func loadWebView() {
        webViewNoticia.navigationDelegate = self
        let myURLString = noticia.url!
        let url = URL(string: myURLString)
        let myRequest = URLRequest(url: url!)
        webViewNoticia.load(myRequest)
    }

    @IBAction func touchButtonFavoritar(_ sender: Any) {
        favorito = favorito ? false : true
        if favorito {
            self.viewModel.favoritarNoticia(noticia: noticia!)
        } else {
            self.viewModel.desfavoritarNoticia(noticia: noticia!)
        }
    }
    
    func reloadFavorito(){
        buttonFavorito.image = !favorito ? UIImage(named: "favorito.png") : UIImage(named: "favoritoSelecionado.png")
    }
    
    func prepareViewModelObserver() {
        
        self.viewModel.noticiaFavoritada = { (finished, error) in
            if !error {
                self.favorito = finished
                self.reloadFavorito()
            } else {
                self.favorito = finished
                self.reloadFavorito()
            }
        }
        
        self.viewModel.noticiaFavoritadaComSucesso = { (finished, error) in
            if !error {
                self.favorito = finished
                self.reloadFavorito()
            } else {
                self.favorito = finished
                self.reloadFavorito()
            }
        }
        
        self.viewModel.noticiaDesfavoritadaComSucesso = { (finished, error) in
            if !error {
                self.favorito = finished
                self.reloadFavorito()
            } else {
                self.favorito = finished
                self.reloadFavorito()
            }
        }
    }
}

extension DetalheNoticiaViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        hideLoading()
        print(error.localizedDescription)
    }
}
