//
//  CadastrarTableViewController.swift
//  Mesa News
//
//  Created by Elton Melo on 24/01/21.
//

import UIKit

class CadastrarTableViewController: UITableViewController {
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    @IBOutlet weak var buttonRegister: UIButton!
    let viewModel = RegisterViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        configTextField()
        hideKeyboardWhenTappedAround()
        prepareViewModelObserver()
    }

    // MARK: - Table view data source

    @IBAction func touchButtonBack(_ sender: Any) {
        goBack()
    }
    
    func configTextField(){
        buttonRegister.layer.cornerRadius = 0.5 * self.buttonRegister.frame.size.height;
        buttonRegister.clipsToBounds = true;
        textFieldName.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        textFieldEmail.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        textFieldPassword.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        textFieldConfirmPassword.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
    }
    
    @IBAction func touchButtonRegister(_ sender: Any) {
        if (textFieldName.text == "") {
            showAlert(title: "Atenção", message: "Campo nome está vázio!", namebutton: "ok", completionHandle: { button in
                
            })
        }else if (textFieldEmail.text == "") {
            showAlert(title: "Atenção", message: "Campo e-mail está vázio!", namebutton: "ok", completionHandle: { button in
                
            })
        } else if (isValidEmail(textFieldEmail.text!) == false) {
            showAlert(title:"Atenção", message: "E-mail inválido!", namebutton: "ok", completionHandle: { button in
                
            })
        } else if (textFieldPassword.text == "") {
            showAlert(title: "Atenção", message: "Campo senha está vázio!" , namebutton: "ok", completionHandle: { button in
                
            })
        } else if (textFieldConfirmPassword.text == "") {
            showAlert(title: "Atenção", message: "Campo confirmar senha está vázio!" , namebutton: "ok", completionHandle: { button in
                
            })
        } else if (textFieldConfirmPassword.text != textFieldPassword.text) {
            showAlert(title: "Atenção", message: "Campo senha e confirmar senha estão diferentes!" , namebutton: "ok", completionHandle: { button in
                
            })
        } else {
            postRegister()
        }
    }
    
    func postRegister() {
        showLoading(msg: "")
        viewModel.postRegister(name: textFieldName.text!, email: textFieldEmail.text!, password: textFieldPassword.text!)
    }
    
    func prepareViewModelObserver() {
        self.viewModel.RegisterFinish = { (finished, error, msg) in
            self.hideLoading()
            if !error {
                self.showAlert(title: "Mesa news", message: msg , namebutton: "ok", completionHandle: { button in
                    self.goBack()
                })
            } else {
                self.showAlert(title: "Atenção", message: msg , namebutton: "ok", completionHandle: { button in
                    
                })
            }
        }
    }
    
}
