//
//  HomeTableViewController.swift
//  Mesa News
//
//  Created by Elton Melo on 26/01/21.
//

import UIKit

class HomeTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var search: UISearchBar!
    let viewModel = HomeViewModel()
    var noticiaSelecionada: Noticia!
    var msgListaVazia = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        prepareViewModelObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getListNew()
    }
    
    func configureView() {
        self.search.delegate = self
        self.search.placeholder = "Pesquisar"
        self.search.showsCancelButton = true
        self.search.alpha = 1
    }

    @IBAction func touchButtonLogout(_ sender: Any) {
        UserDefaults.standard.setToken(token: "")
        performSegue(withIdentifier: "screenLogin", sender: nil)
    }
    
    func getListNew() {
        showLoading(msg: "")
        viewModel.getNoticias()
    }
    
    func prepareViewModelObserver() {
        self.viewModel.getNewsFinish = { (finished, error, msg) in
            self.hideLoading()
            if !error {
                self.tableView.reloadData()
                self.msgListaVazia = ""
            } else {
                self.msgListaVazia = msg
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.listaFiltrada.count == 0 {
            self.tableView.setEmptyMessage(msgListaVazia)
        } else {
            self.tableView.restore()
        }
        return viewModel.listaFiltrada.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticiaTableViewCell", for: indexPath) as! NoticiaTableViewCell
        let noticia = viewModel.listaFiltrada[indexPath.row]
        cell.tableView = self
        cell.noticiaItem = noticia
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        noticiaSelecionada = viewModel.listaFiltrada[indexPath.row]
        performSegue(withIdentifier: "detalhe", sender: nil)
    }
    
    // MARK: - Delegate busca
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.search.endEditing(true)
        viewModel.listaFiltrada = viewModel.newsResponse!.data
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.search.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (self.search.text!.isEmpty) {
            viewModel.listaFiltrada = viewModel.newsResponse!.data
        } else {
            viewModel.listaFiltrada = (viewModel.newsResponse?.data.filter({(midia: Noticia) -> Bool in
                
                let stringMatch = midia.title.lowercased().range(of:searchText.lowercased())
                let result = stringMatch != nil ? true : false
                return result
            }))!
        }
        self.tableView.reloadData()
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detalhe" {
            if let destinationVC = segue.destination as? DetalheNoticiaViewController {
                destinationVC.noticia = noticiaSelecionada
            }
        }
    }
    

}
